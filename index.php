<?php
$url = 'https://v3-static.supadu.io/dev/products/9780060577315.json';

// Read Json file and decode
$content = file_get_contents($url);
$json = json_decode($content, true);
// Set the contents file to a variabile
?>


<!DOCTYPE html>
<html lang="en">
<!-- Set language to English -->



<head>
    <title>Frontend Engineer Coding Challenge</title>
    <meta charset="utf-8">
    <!-- Set characters to be from 1 to 4 bytes long -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Setup View port -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!-- Bring in bootstrap style sheet -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Bring in jquery javascript include for font resizing and Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- Bring in javascript include for Bootstrap -->


    <style>
        @import url('https://fonts.googleapis.com/css2?family=Bitter:wght@500&display=swap');

        /* Import google font*/


        h1 {
            font-family: 'Bitter', serif;
            font-size: 80px;
            text-shadow: 1px 1px #555555;
        }

        /* Set font and size for H1 */

        p {
            font-family: 'Bitter', serif;
            font-size: 16px;
        }

        /* Set font and size for p */


        h3 {
            font-family: 'Bitter', serif;
            font-size: 20px;
            font-weight: 900;
            margin-top: 0px;
        }

        /* Set font and size for p */
    </style>





</head>

<body>

    <div class="jumbotron text-center">
        <h1><?php echo $json['title']; ?></h1>
        <!-- Display book title -->
        <p>Release Date: <?php $date = new DateTime($json['sale_date']['date']); echo $date->format('d-m-Y'); ?>
            <!-- Display book release date and convert to a readable format -->
            Price: <?php foreach($json['prices'] as $item) { echo $item['locale']; echo " $". $item['amount'] . " "; }?>
        </p>

        <!-- Increase and Descrease font size for accessibility-->
        <button id="btn-decrease">A-</button><button id="btn-orig">A</button><button id="btn-increase">A+</button>

    </div>

    <!-- Build container for bootstrap grid -->
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <img src=" <?php echo $newimage = substr($json['image'], 0, -7) . '400.jpg' ?>" class="rounded" width="100%" alt="Book Cover">
                <!-- Display book cover image -->
            </div>
            <div class="col-sm-4">
                <?php echo $json['description'];?>
                <!-- Display description about the book -->
            </div>
            <div class="col-sm-4">
                <h3>Author: <?php foreach($json['contributors'] as $item){ echo $item['contributor']['name']; }?></h3>
                <p><?php foreach($json['contributors'] as $item){echo $item['contributor']['bio'];}?></p>
                <!-- Display contributors using a loop until end of element  -->
            </div>
            <div class="col-sm-4">
                <h3>Book Reviews</h3>
                <p><?php foreach($json['reviews'] as $item){echo $item['review']['reviewer'];?> </p>
                <p><?php echo $item['review']['description'];}?></p>
                <!-- Display book reviews using a loop until end of element  -->
            </div>

            <div class="col-sm-4">
                <h3>Available from these sellers</h3>
                <p><?php foreach($json['retailers'] as $item) { ?>
                    <?php $url = $item['path'];
                   parse_str(parse_url($url)['query'], $params); ?>
                    <a href="<?php echo $item['path'];?>"><?php echo ucwords($params['retailer']);?></a>
                    <?php } ?>
                </p>
                <!-- Display sellers list using a loop until end of element and convert the first character of each word to uppercase-->
            </div>
        </div>
    </div>



    <script>
        var $buttonElements = $("p, H3, H1");
        // font size will increase on p H1 and M3 tags

        $buttonElements.each(function () {
            var $this = $(this);
            $this.data("orig-size", $this.css("font-size"));
        });

        $("#btn-increase").click(function () {
            changeFontSize(1);
        })

        /// Increase font size by 1 on click

        $("#btn-decrease").click(function () {
            changeFontSize(-1);
        })

        /// Dencrease font size by 1 on click

        $("#btn-orig").click(function () {
            $buttonElements.each(function () {
                var $this = $(this);
                $this.css("font-size", $this.data("orig-size"));
            });
        })

        // Return to correct font size 

        function changeFontSize(direction) {
            $buttonElements.each(function () {
                var $this = $(this);
                $this.css("font-size", parseInt($this.css("font-size")) + direction);
            });
        }

        // Change font size direction 
    </script>



</body>

</html>